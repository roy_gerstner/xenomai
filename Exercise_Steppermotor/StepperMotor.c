#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <alchemy/task.h>
#include <alchemy/timer.h>
#include <rtdm/gpio.h>
#include <alchemy/queue.h>
#include <alchemy/alarm.h>

const int _ms = 1000000llu;
const int _us = 1000llu;
const int MAX_PWM_DUTY_CYCLE = 100;
const int MIN_PWM_DUTY_CYCLE = 0;
const int MAX_CYCLE_COUNTER = 50;

enum command{
	CMD_EXIT = 0,
	CMD_PWM_DUTY_CYCLE = 1,
	CMD_PRINT_STATISTICS = 2,
	CMD_INCREMENT_SPEED = 3,
	CMD_DECREMENT_SPEED = 4
};

RT_TASK task_Steppermotor;
RT_QUEUE msgq_UserInput;

struct MsgUserInput{
	int pwmDutyCycle;
	command cmd;
};

int checkMesgQ(MsgUserInput &recMsg){
	int ret = 0;
	ret = rt_queue_read(&msgq_UserInput, &recMsg, sizeof(MsgUserInput), 1);
	// rt_printf("rt_queue_read() = %d\n", ret);
	return ret;
}


// print timing statistics
void printStatistics(int diffTime[]){
	printf("Cycle times:\n");
	for (int i = 0; i < MAX_CYCLE_COUNTER; i++) {
		printf("CycleTime[%d] = %lldns\n", i, diffTime[i]);
	}
}
// function to be executed by task
void stepperMotorFunc(void *arg) {
	RT_TASK_INFO curtaskinfo;
	RTIME period = 1000 * _us;

	MsgUserInput recMsg;

	int fd[4];
	int initValue = 0;
	int ret = 0;
	int pwmDutyCycle = 0; //[ticks]
	int counter = 0;

	const int motorPhases[8][4] = { // [phase][pin]
	  // -------- pins ----------
	  // Winding    A  B  A  B
	  // Motor Pin  1  2  3  4
	  // Color      Bl Pi Ye Or
	  {             1, 1, 0, 0},
	  {             0, 1, 0, 0},
	  {             0, 1, 1, 0},
	  {             0, 0, 1, 0},
	  {             0, 0, 1, 1},
	  {             0, 0, 0, 1},
	  {             1, 0, 0, 1},
	  {             1, 0, 0, 0}
	};

	rt_printf("Call Stepper Motor Task()!\n");

	// inquire current task
	rt_task_inquire(NULL, &curtaskinfo);

	ret = rt_task_set_priority(NULL, 20);
	rt_printf("rt_task_set_priority() = %d: \n", ret);

	// print task name
	rt_printf("Task name : %s \n", curtaskinfo.name);
	rt_printf("Task prio : %d \n", curtaskinfo.prio);
	rt_printf("Task pid : %d \n", curtaskinfo.pid);
	rt_printf("Task stat : %d \n", curtaskinfo.stat);

	ret = rt_task_set_periodic(NULL, TM_NOW, period);
	rt_printf("rt_task_set_periodic(%ld) = %d: \n", period, ret);

	printf("Open GPIOs 17,27,22, 24\n");
	fd[0] = open("/dev/rtdm/pinctrl-bcm2835/gpio17", O_WRONLY);
	printf("open() %d\n", fd[0]);
	fd[1] = open("/dev/rtdm/pinctrl-bcm2835/gpio27", O_WRONLY);
	printf("open() %d\n", fd[1]);
	fd[2] = open("/dev/rtdm/pinctrl-bcm2835/gpio22", O_WRONLY);
	printf("open() %d\n", fd[2]);
	fd[3] = open("/dev/rtdm/pinctrl-bcm2835/gpio24", O_WRONLY);
	printf("open() %d\n", fd[3]);

	if (fd[0] > 0 && fd[1] > 0 && fd[2] > 0 && fd[3] > 0) {
		printf("set GPIOs for output!\n");
		//set device to output mode with special device request GPIO_RTIOC_DIR_OUT
		ret=ioctl(fd[0], GPIO_RTIOC_DIR_OUT, &initValue);
		printf("ioctl() %d\n", ret);
		ret=ioctl(fd[1], GPIO_RTIOC_DIR_OUT, &initValue);
		printf("ioctl() %d\n", ret);
		ret=ioctl(fd[2], GPIO_RTIOC_DIR_OUT, &initValue);
		printf("ioctl() %d\n", ret);
		ret=ioctl(fd[3], GPIO_RTIOC_DIR_OUT, &initValue);
		printf("ioctl() %d\n", ret);
	}
	else{
		return;
	}

	ret = write(fd[0], &motorPhases[0][0], sizeof(int));
	printf("write(%d) %d\n", motorPhases[0][0], ret);
	ret = write(fd[1], &motorPhases[0][1], sizeof(int));
	printf("write(%d) %d\n", motorPhases[0][1], ret);
	ret = write(fd[2], &motorPhases[0][2], sizeof(int));
	printf("write(%d) %d\n", motorPhases[0][2], ret);
	ret = write(fd[3], &motorPhases[0][3], sizeof(int));
	printf("write(%d) %d\n", motorPhases[0][3], ret);

	//Wait for start
	ret = rt_task_wait_period(NULL);
	rt_printf("rt_task_wait_period = %d: \n", ret);

	while (1) {
		counter = counter % 8;
		printf("Next step(%d) \n", counter);
		ret = write(fd[0], &motorPhases[counter][0], sizeof(int));
		// printf("write(%d) %d\n", motorPhases[counter][0], ret);
		ret = write(fd[1], &motorPhases[counter][1], sizeof(int));
		// printf("write(%d) %d\n", motorPhases[counter][1], ret);
		ret = write(fd[2], &motorPhases[counter][2], sizeof(int));
		// printf("write(%d) %d\n", motorPhases[counter][2], ret);
		ret = write(fd[3], &motorPhases[counter][3], sizeof(int));
		// printf("write(%d) %d\n", motorPhases[counter][3], ret);

		ret = checkMesgQ(recMsg);
		if(ret > 0){
			// Msg received
			rt_printf("Msg.cmd = %d\n", recMsg.cmd);
			rt_printf("Msg.pwmDutyCycle = %d\n", recMsg.pwmDutyCycle);

			if(recMsg.pwmDutyCycle > 0){
				pwmDutyCycle = period * recMsg.pwmDutyCycle / 100;
			}
			else{
				recMsg.pwmDutyCycle = 0;
			}
			rt_printf("pwmDutyCycle = %d\n", pwmDutyCycle);
		}
		ret = rt_task_wait_period(NULL);


		counter++;
	}
	rt_printf("End Time = %dns\n", rt_timer_read());

	return;
}

int main(int argc, char *argv[]) {
	char str[10];
	int ret = -1;

	char userInput;
	int pwmDutyCycle = 0; // [%]
	MsgUserInput* pTmpMsg;

	printf("create & start task\n");
	sprintf(str, "stepper");

	// Create MsgQ
	ret = rt_queue_create(&msgq_UserInput, "MSGQ_USER_INPUT", 1000, 100, Q_FIFO);
	printf("rt_queue_create() %d\n", ret);

	/* Create task
	 * Arguments: &task,
	 *            name,
	 *            stack size (0=default),
	 *            priority,
	 *            mode (FPU, start suspended, ...)
	 */
	ret = rt_task_create(&task_Steppermotor, str, 200000, 50, 0);
	printf("rt_task_create() %d\n", ret);

	printf("rt_task_start() ");
	ret = rt_task_start(&task_Steppermotor, &stepperMotorFunc, 0);
	printf("%d\n", ret);

	while (1) {
		//printf(".");
		char ch;
		char buf[2];


		printf("Wait for input : ");
		fgets(buf, 2, stdin);
		switch (buf[0]) {
		case 'q': // exit application
			pTmpMsg = (MsgUserInput*) rt_queue_alloc(&msgq_UserInput, sizeof(MsgUserInput));
			printf("rt_queue_alloc(%d) %d\n", sizeof(MsgUserInput), pTmpMsg);

			pTmpMsg->cmd = CMD_EXIT;

			ret = rt_queue_send(&msgq_UserInput, pTmpMsg, sizeof(MsgUserInput), Q_NORMAL);
			printf("rt_queue_send() %d\n", ret);
			sleep(2);
			return 0;
			break;
		case '+': //increment PWM duty cycle
			pwmDutyCycle += 10;
			if (pwmDutyCycle > MAX_PWM_DUTY_CYCLE) {
				pwmDutyCycle = MAX_PWM_DUTY_CYCLE;
			}
			pTmpMsg = (MsgUserInput*) rt_queue_alloc(&msgq_UserInput, sizeof(MsgUserInput));

			pTmpMsg->cmd = CMD_PWM_DUTY_CYCLE;
			pTmpMsg->pwmDutyCycle = pwmDutyCycle;

			ret = rt_queue_send(&msgq_UserInput, pTmpMsg, sizeof(MsgUserInput), Q_NORMAL);
			printf("rt_queue_send() %d\n", ret);
			break;
		case '-': //decrement PWM duty cycle
			pwmDutyCycle -= 10;
			if (pwmDutyCycle < MIN_PWM_DUTY_CYCLE) {
				pwmDutyCycle = MIN_PWM_DUTY_CYCLE;
			}
			pTmpMsg = (MsgUserInput*) rt_queue_alloc(&msgq_UserInput, sizeof(MsgUserInput));

			pTmpMsg->cmd = CMD_PWM_DUTY_CYCLE;
			pTmpMsg->pwmDutyCycle = pwmDutyCycle;

			ret = rt_queue_send(&msgq_UserInput, pTmpMsg, sizeof(MsgUserInput), Q_NORMAL);
			printf("rt_queue_send() %d\n", ret);
			break;
		default: // ingnor unknown characters
			break;
		}
		printf("PWM duty cycle = %d\n", pwmDutyCycle);
	}
	printf("Main End Time = %dns\n", rt_timer_read());
}
