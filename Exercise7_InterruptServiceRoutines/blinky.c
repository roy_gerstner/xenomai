#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <alchemy/task.h>
#include <alchemy/timer.h>
#include <rtdm/gpio.h>
#include <alchemy/queue.h>
#include <alchemy/alarm.h>

const int _ms = 1000000llu;
const int _us = 1000llu;
const int MAX_PWM_DUTY_CYCLE = 100;
const int MIN_PWM_DUTY_CYCLE = 0;

enum command{
	CMD_EXIT = 0,
	CMD_PWM_DUTY_CYCLE = 1,
	CMD_PRINT_STATISTICS = 2
};

RT_TASK task_Steppermotor;
RT_QUEUE msgq_UserInput;
RT_ALARM timer_DutyCycle;

struct MsgUserInput{
	int pwmDutyCycle;
	command cmd;
};

int MAX_CYCLE_COUNTER = 50;
int fd;

int checkMesgQ(MsgUserInput &recMsg){
	int ret = 0;
	ret = rt_queue_read(&msgq_UserInput, &recMsg, sizeof(MsgUserInput), 1);
	// rt_printf("rt_queue_read() = %d\n", ret);
	return ret;
}

//int forwardUserInput(MsgUserInput pTmpMsg, int size){
//	int ret = 0;
//	pTmpMsg = (MsgUserInput*) rt_queue_alloc(&msgq_UserInput, sizeof(MsgUserInput));
//	ret = rt_queue_write(&msgq_UserInput, buf, size, Q_NORMAL);
//	return ret;
//}

// SW timer interrupt function
void sw_isrTimerDutyCycle(void *arg){
	// Shutdown GPIO23
	int value = 0;
	write(fd, &value, sizeof(value));
}


// print timing statistics
void printStatistics(int diffTime[]){
	printf("Cycle times:\n");
	for (int i = 0; i < MAX_CYCLE_COUNTER; i++) {
		printf("CycleTime[%d] = %lldns\n", i, diffTime[i]);
	}
}
// function to be executed by task
void blinkyFunc(void *arg) {
	RT_TASK_INFO curtaskinfo;
	RTIME period = 1 * _ms;

	int pwmDutyCycle = 0; //[ticks]
	int value = 0;
	RTIME diffTime[MAX_CYCLE_COUNTER];
	RTIME lastCycleTime, now;
	int counter = 0;
	int ret = -1;
	MsgUserInput recMsg;

	rt_printf("Call blinkyTask()!\n");

	// inquire current task
	rt_task_inquire(NULL, &curtaskinfo);

	ret = rt_task_set_priority(NULL, 20);
	rt_printf("rt_task_set_priority() = %d: \n", ret);

	// print task name
	rt_printf("Task name : %s \n", curtaskinfo.name);
	rt_printf("Task prio : %d \n", curtaskinfo.prio);
	rt_printf("Task pid : %d \n", curtaskinfo.pid);
	rt_printf("Task stat : %d \n", curtaskinfo.stat);

	ret = rt_task_set_periodic(NULL, TM_NOW, period);
	rt_printf("rt_task_set_periodic(%ld) = %d: \n", period, ret);

	// Create Alarm for Duty Cycle
	ret = rt_alarm_create(&timer_DutyCycle, "TimerDutyCycle", sw_isrTimerDutyCycle, NULL);
	rt_printf("rt_alarm_create() = %d\n", ret);

	//Wait for start
	ret = rt_task_wait_period(NULL);
	rt_printf("rt_task_wait_period = %d: \n", ret);

	while (1) {
		/*
		 Toggle GPIO
		 */
		lastCycleTime = rt_timer_read();
		ret = write(fd, &value, sizeof(value));

		// start sw timer
		if( pwmDutyCycle > 0){
			rt_alarm_start(&timer_DutyCycle, pwmDutyCycle, TM_INFINITE);
		}
		else{
			// turm off GPIO
		}

		ret = checkMesgQ(recMsg);
		if(ret > 0){
			// Msg received
			rt_printf("Msg.cmd = %d\n", recMsg.cmd);
			rt_printf("Msg.pwmDutyCycle = %d\n", recMsg.pwmDutyCycle);

			if(recMsg.pwmDutyCycle > 0){
				pwmDutyCycle = period * recMsg.pwmDutyCycle / 100;
				value = 1;
			}
			else{
				recMsg.pwmDutyCycle = 0;
				value = 0;
			}
			rt_printf("pwmDutyCycle = %d\n", pwmDutyCycle);
		}
		ret = rt_task_wait_period(NULL);
		now = rt_timer_read();
		diffTime[counter] = now - lastCycleTime;
		counter++;
		if( counter >= MAX_CYCLE_COUNTER ){
			counter = 0;
		}
		//printf("%d(ret=%d): Toggle Pin %ld\n", counter, ret, now);
	}
	rt_printf("End Time = %dns\n", rt_timer_read());

	return;
}

int main(int argc, char *argv[]) {
	char str[10];
	int ret = -1;
	int initValue = 0;
	char userInput;
	int pwmDutyCycle = 0; // [%]
	MsgUserInput* pTmpMsg;

	printf("create & start task\n");
	sprintf(str, "blinky");

	/* Create task
	 * Arguments: &task,
	 *            name,
	 *            stack size (0=default),
	 *            priority,
	 *            mode (FPU, start suspended, ...)
	 */
	ret = rt_task_create(&task_Steppermotor, str, 200000, 50, 0);
	printf("rt_task_create() %d\n", ret);

	// Create MsgQ
	ret = rt_queue_create(&msgq_UserInput, "MSGQ_USER_INPUT", 1000, 100,
	Q_FIFO);
	printf("rt_queue_create() %d\n", ret);

	printf("Open GPIO2 device!\n");
	fd = open("/dev/rtdm/pinctrl-bcm2835/gpio2", O_WRONLY);
	printf("open() %d\n", fd);
	if (fd > 0) {
		printf("set GPIO23 for output!\n");
		//set device to output mode with special device request GPIO_RTIOC_DIR_OUT
		ret=ioctl(fd, GPIO_RTIOC_DIR_OUT, &initValue);
		printf("ioctl() %d\n", ret);
		if (ret == 0) {
			/*  Start task
			 * Arguments: &task,
			 *            task function,
			 *            function argument
			 */
			printf("rt_task_start() ");
			ret = rt_task_start(&task_Steppermotor, &blinkyFunc, 0);
			printf("%d\n", ret);
		}
	}
	while (1) {
		//printf(".");
		char ch;
		char buf[2];


		printf("Wait for input : ");
		fgets(buf, 2, stdin);
		switch (buf[0]) {
		case 'q': // exit application
			pTmpMsg = (MsgUserInput*) rt_queue_alloc(&msgq_UserInput, sizeof(MsgUserInput));
			printf("rt_queue_alloc(%d) %d\n", sizeof(MsgUserInput), pTmpMsg);

			pTmpMsg->cmd = CMD_EXIT;

			ret = rt_queue_send(&msgq_UserInput, pTmpMsg, sizeof(MsgUserInput), Q_NORMAL);
			printf("rt_queue_send() %d\n", ret);
			sleep(2);
			return 0;
			break;
		case '+': //increment PWM duty cycle
			pwmDutyCycle += 10;
			if (pwmDutyCycle > MAX_PWM_DUTY_CYCLE) {
				pwmDutyCycle = MAX_PWM_DUTY_CYCLE;
			}
			pTmpMsg = (MsgUserInput*) rt_queue_alloc(&msgq_UserInput, sizeof(MsgUserInput));

			pTmpMsg->cmd = CMD_PWM_DUTY_CYCLE;
			pTmpMsg->pwmDutyCycle = pwmDutyCycle;

			ret = rt_queue_send(&msgq_UserInput, pTmpMsg, sizeof(MsgUserInput), Q_NORMAL);
			printf("rt_queue_send() %d\n", ret);
			break;
		case '-': //decrement PWM duty cycle
			pwmDutyCycle -= 10;
			if (pwmDutyCycle < MIN_PWM_DUTY_CYCLE) {
				pwmDutyCycle = MIN_PWM_DUTY_CYCLE;
			}
			pTmpMsg = (MsgUserInput*) rt_queue_alloc(&msgq_UserInput, sizeof(MsgUserInput));

			pTmpMsg->cmd = CMD_PWM_DUTY_CYCLE;
			pTmpMsg->pwmDutyCycle = pwmDutyCycle;

			ret = rt_queue_send(&msgq_UserInput, pTmpMsg, sizeof(MsgUserInput), Q_NORMAL);
			printf("rt_queue_send() %d\n", ret);
			break;
		default: // ingnor unknown characters
			break;
		}
		printf("PWM duty cycle = %d\n", pwmDutyCycle);
	}
	printf("Main End Time = %dns\n", rt_timer_read());
}
