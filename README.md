# Xenomai

Xenomai examples und exercices from http://www.cs.ru.nl/lab/xenomai/

Exercise7_InterruptServiceRoutines
    Exercise #7 8extended): Interrupt Service Routines:
    Extended excercice #7 with software PWM generation to control the brigthness of an LED on PIN2 of the Raspberry PI.
    The duty cycle can be increased/decreased with +/- keys by increments of 10%.

Exercise_Steppermotor (BYJ48 Stepper Motor)
    Example to drive a stepper motor on PIN 17/27/22/24 of the Raspberry PI
    The program executes a fixed step sequnce of 8 half steps in a cycle auf 1ms.
    With 4096 steps per revolution this gives us a max. of about 15 rev/min
